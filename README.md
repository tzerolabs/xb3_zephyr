# WHAT

This is a driver for the zephyr-rtos for the xbee3 iot modem.
This takes advantage of the socket offload functionality of the zephyr net stack
and allows for the usual bsd socket api to be used.

# HOW

in order to use this in a zephyr project. this repository/project must be specified
in the west.yml as a dependancy.
