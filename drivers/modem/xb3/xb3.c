#include "sys/byteorder.h"
#include "sys/thread_stack.h"
#define DT_DRV_COMPAT digi_xbee3_lte

#include <logging/log.h>
LOG_MODULE_REGISTER(modem_xb3, CONFIG_XB3_LOG_LEVEL);

#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <zephyr.h>
#include <drivers/gpio.h>
#include <device.h>
#include <init.h>
#include <drivers/uart.h>
#include <kernel.h>

#include <net/net_if.h>
#include <net/net_ip.h>
#include <net/net_pkt.h>
#include <net/net_offload.h>
#include <net/socket_offload.h>

#include <sys/ring_buffer.h>

#include "modem_context.h"
#include "modem_cmd_handler.h"
#include "modem_socket.h"
#include "modem_iface_uart.h"

/*******************************************************************************
 * DEFINITIONS
 ******************************************************************************/
#define MDM_CMD_TIMEOUT			K_SECONDS(10)
#define MDM_DNS_TIMEOUT			K_SECONDS(70)
#define MDM_CMD_CONN_TIMEOUT		K_SECONDS(120)
#define MDM_REGISTRATION_TIMEOUT	K_SECONDS(180)
#define MDM_PROMPT_CMD_DELAY		K_MSEC(75)
#define MDM_SENDMSG_SLEEP		K_MSEC(1)

#define MDM_MAX_DATA_LENGTH		512
#define MDM_RECV_MAX_BUF		30
#define MDM_RECV_BUF_SIZE		128

#define MDM_MAX_SOCKETS			6
#define MDM_BASE_SOCKET_NUM		0

#define MDM_NETWORK_RETRY_COUNT		3
#define MDM_WAIT_FOR_RSSI_COUNT		60
#define MDM_WAIT_FOR_RSSI_DELAY		K_SECONDS(2)

#define BUF_ALLOC_TIMEOUT		K_SECONDS(1)

#define MDM_MANUFACTURER_LENGTH		10
#define MDM_MODEL_LENGTH		16
#define MDM_REVISION_LENGTH		64
#define MDM_IMEI_LENGTH			32
#define MDM_IMSI_LENGTH			32
#define MDM_ICCID_LENGTH		32
#define MDM_PH_LENGTH			32
#define RSSI_TIMEOUT_SECS		30

#define xb3_START_DELIM			0x7E


#define FAIL_IF_RET(var, retval) if(var) return retval;

#define FAIL_IF_LOG(test, retval, lvl, fmt, ...) \
	if(test) { 							\
		LOG_##lvl(fmt, ##__VA_ARGS__); 				\
		return retval; 						\
	}

#define FAIL_IF_ERR(test, retval, fmt, ...) 				\
	FAIL_IF_LOG(test, retval, ERR, fmt, ##__VA_ARGS__)
#define FAIL_IF_WRN(test, retval, fmt, ...) 				\
	FAIL_IF_LOG(test, retval, WRN, fmt, ##__VA_ARGS__)
#define FAIL_IF_INF(test, retval, fmt, ...) 				\
	FAIL_IF_LOG(test, retval, INF, fmt, ##__VA_ARGS__)
#define FAIL_IF_DBG(test, retval, fmt, ...) 				\
	FAIL_IF_LOG(test, retval, DBG, fmt, ##__VA_ARGS__)

#define FAIL_IF(var, fmt, ...) FAIL_IF_RET(var, -1, fmt, ##__VA_ARGS__)

#define GOTO_IF(test, label) if(test) goto label;
#define GOTO_IF_LOG(test, label, lvl, fmt, ...) \
	if(test) { 							\
		LOG_##lvl(fmt, ##__VA_ARGS__); 				\
		goto label; 						\
	}

#define GOTO_IF_ERR(test, label, fmt, ...) 				\
	GOTO_IF_LOG(test, label, ERR, fmt, ##__VA_ARGS__)
#define GOTO_IF_WRN(test, label, fmt, ...) 				\
	GOTO_IF_LOG(test, label, WRN, fmt, ##__VA_ARGS__)
#define GOTO_IF_INF(test, label, fmt, ...) 				\
	GOTO_IF_LOG(test, label, INF, fmt, ##__VA_ARGS__)
#define GOTO_IF_DBG(test, label, fmt, ...) 				\
	GOTO_IF_LOG(test, label, DBG, fmt, ##__VA_ARGS__)


enum mdm_control_pins {
	MDM_RESET,
	MDM_DTR,
	MDM_MAX_PINS
}; 

const bool mdm_pin_active_val[MDM_MAX_PINS] = {
	0, //RESET
	0, //DTR
};

uint32_t xb3_baud_rates[] = {
	1200,
	2400,
	4800,
	9600,
	19200,
	38400,
	57600,
	115200,
	230400,
};


typedef int (*xb3_api_handler_fun)(uint8_t frame_id, uint8_t *data, 
		uint16_t length, struct modem_context *mctx);
struct xb3_api_handler {
	xb3_api_handler_fun func;
	uint8_t frame_id;
};



#define xb3_API_FUNC(_name) 						\
	int _name(uint8_t frame_id, uint8_t *data, uint16_t length,  	\
		struct modem_context *mctx) 				

#define xb3_API_HANDLER(_name, _frame_id) 				\
	xb3_API_FUNC(_name##_func); 					\
	static const struct xb3_api_handler _name =  { 			\
		.func=_name##_func, 					\
		.frame_id=_frame_id}; 					\
	xb3_API_FUNC(_name##_func) 					


/* socket read callback data */
struct socket_read_data {
	struct net_pkt *pkt;
	uint8_t buf[MDM_MAX_DATA_LENGTH * CONFIG_MODEM_SOCKET_PACKET_COUNT];
	struct ring_buf rb;
	size_t avail;
	struct sockaddr recv_addr;
	uint16_t recv_read_len;
	uint8_t sock_id;
	struct k_sem client_connection;
};


//rom data
struct modem_config {
	uint32_t baud;
	const char *bus_name;
};

/* ram data */
struct modem_data {
	const struct device *dev;
	struct net_if *net_iface;

	struct modem_context ctx;
	struct modem_iface_uart_data iface_data;
	struct modem_cmd_handler_data cmd_handler_data;

	char mdm_match_buf[MDM_MAX_DATA_LENGTH + 11];
	struct modem_pin pins[MDM_MAX_PINS];

	char mdm_man[MDM_MANUFACTURER_LENGTH];
	char mdm_model[MDM_MODEL_LENGTH];
	char mdm_rev[MDM_REVISION_LENGTH];
	char mdm_imei[MDM_IMEI_LENGTH];
	char mdm_ph_num[MDM_PH_LENGTH];
	char mdm_iccid[MDM_ICCID_LENGTH];
	char mdm_imsi[MDM_IMSI_LENGTH];

	uint8_t mac_addr[6];
	/* modem interface */
	/* modem cmds */
	uint8_t at_buf[MDM_RECV_BUF_SIZE + 1];
	char mdm_iface_rb[32];

	/* socket data */
	struct socket_read_data sock_data[MDM_MAX_SOCKETS];
	struct modem_socket sockets[MDM_MAX_SOCKETS];
	struct modem_socket_config socket_cfg;

	/* modem state */
	int ev_creg;
	int cell_registered_p;
	int ble_connect_p;

	/* response semaphore */
	struct k_sem sem_response;

	K_KERNEL_STACK_MEMBER(rx_stack, CONFIG_MODEM_XB3_RX_STACK_SIZE);
	struct k_thread thread_data;

	/* RSSI work */
	struct k_delayed_work rssi_query_work;
};


#define DATA_FROM_DEV(dev) ((struct modem_data*)dev->data)
#define CFG_FROM_DEV(dev) ((struct modem_config*)dev->config)
#define DATA_FROM_CTX(_ctx) ((struct modem_data*)_ctx->driver_data)
#define DEV_FROM_CTX(_ctx) (DATA_FROM_CTX(_ctx)->dev)
#define CFG_FROM_CTX(_ctx) (CFG_FROM_DEV(DEV_FROM_CTX(_ctx)))

//convienence macros to keep frame id/file descriptor straight
#define FD_TO_FRAME_ID(fd) (fd + 1)
#define FRAME_ID_TO_FD(id) (id - 1)

#define AT_CMD_FRAME_ID 	(255)

/*******************************************************************************
 * UTILITY FUNCTIONS
 ******************************************************************************/

static uint8_t calc_api_checksum(uint8_t *data, uint16_t length)
{
	uint8_t chksum = 0;
	for(int i = 0; i < length; i++){
		chksum += data[i];
	}

	return 0xFF - chksum;
}

static struct net_pkt* alloc_api_buf(struct modem_context *mctx, size_t len){
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct net_pkt *buf = net_pkt_alloc_with_buffer(mdata->net_iface, len, 
			AF_UNSPEC, 0, MDM_CMD_TIMEOUT);

	return buf;
}


static int send_api(
		struct modem_context *mctx,
		struct net_pkt *pkt,
		k_timeout_t timeout)
{
	int ret = 0;

	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	uint8_t preamble[3];

	//move cursor to beginning of packet
	net_pkt_cursor_init(pkt);
	uint16_t length = net_pkt_remaining_data(pkt);

	preamble[0] = xb3_START_DELIM;
	*((uint16_t*)&preamble[1]) = sys_cpu_to_be16(length);

	uint8_t buf[length];

	ret = net_pkt_read(pkt, buf, length);

	ret = k_sem_take(&mdata->cmd_handler_data.sem_tx_lock, timeout);
	if(ret == -EAGAIN){
		ret = -ETIMEDOUT;
		goto exit;
	}

	//checksum does not include start delim or length
	uint8_t checksum = calc_api_checksum(buf, length);

	mctx->iface.write(&mctx->iface, preamble, 3);
	mctx->iface.write(&mctx->iface, buf, length);
	mctx->iface.write(&mctx->iface, &checksum, 1);


	//no timeout, dont worry about response.
	if(K_TIMEOUT_EQ(timeout, K_NO_WAIT)){
		ret = 0;
		goto exit;
	}

	//wait for a response handler to give the sem
	k_sem_reset(&mdata->sem_response);
	ret = k_sem_take(&mdata->sem_response, timeout);

	if(ret == 0){
		ret = modem_cmd_handler_get_error(&mdata->cmd_handler_data);
	}else if(ret == -EAGAIN){
		ret = -ETIMEDOUT;
	}
exit:

	k_sem_give(&mdata->cmd_handler_data.sem_tx_lock);
	net_pkt_unref(pkt);
	return ret;
}


static int _send_api_at (struct modem_context *mctx, const char cmd[2],
		char *param, uint8_t param_size, 
		char *resp, uint16_t resp_size)
{
	int ret = 0;
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct net_pkt *buf = alloc_api_buf(mctx, param_size + 4);
	uint8_t frame_id = (resp == NULL) ? 0 : AT_CMD_FRAME_ID;

	if(!buf){
		errno = ENOMEM;
		ret = -1;
		goto err;
	}


	net_pkt_write_u8(buf, 0x08);
	net_pkt_write_u8(buf, frame_id);
	net_pkt_write(buf, cmd, 2);

	if(param){
		net_pkt_write(buf, param, param_size);
	}

	k_timeout_t timeout = (resp == NULL) ? K_NO_WAIT:MDM_CMD_TIMEOUT;
	errno = send_api(mctx, buf, timeout);

	if(errno){
		ret = -1;
		goto err;
	}

	if(resp){
		uint16_t len;
		memset(resp, '\0', resp_size);
		memcpy(&len, mdata->at_buf, sizeof(len));
		if(len > resp_size){
			LOG_ERR("modem response is too long (%d/%d)", 
					len, resp_size);
			len = resp_size - 1;
		}

		memcpy(resp, &mdata->at_buf[2], len);
	}
err:
	return errno;
}

#define send_api_at(mctx, cmd, resp, resp_len) \
	_send_api_at(mctx, cmd, NULL, 0, resp, resp_len)
#define send_api_at_no_resp(mctx, cmd) \
	_send_api_at(mctx, cmd, NULL, 0, NULL, 0)

#define send_api_at_param(mctx, cmd, param, param_len, resp, resp_len) \
	_send_api_at(mctx, cmd, param, param_len, resp, resp_len)
#define send_api_at_param_no_resp(mctx, cmd, param, param_len) \
	_send_api_at(mctx, cmd, param, param_len, NULL, 0)



/*******************************************************************************
 * AT COMMAND HANDLERS
 ******************************************************************************/
//this is cheating... but necessary. Unfortunately the command handler
//does not pass back a pointer to the modem context or device struct 
//or even a pointer to the semaphore passed to it.
//therefore, in order to be able to give back said semaphore we need
//to cheat and look up the device pointer of the first (and hopefully only)
//xb3 modem on the system.
static struct modem_data mdata_0;

/*
 * Modem Response Command Handlers
 */
/* Handler: OK */
MODEM_CMD_DEFINE(on_cmd_ok)
{
	modem_cmd_handler_set_error(data, 0);
	k_sem_give(&mdata_0.sem_response);
	return 0;
}

/* Handler: ERROR */
MODEM_CMD_DEFINE(on_cmd_error)
{
	modem_cmd_handler_set_error(data, -EIO);
	k_sem_give(&mdata_0.sem_response);
	return 0;
}


/*******************************************************************************
 * API COMMAND HANDLERS
 ******************************************************************************/

/* handler for unsolicited modem status messages */
xb3_API_HANDLER(on_modem_status, 0x8A)
{
	char *msg;
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	//this is an oddball api frame that doesn't have a frame_id byte.
	//so this implementation puts the status byte into the frame_id place.
	switch(frame_id){
		case 0x00:
			msg = "Hardware reset or power up";
			break;
		case 0x01:
			msg = "Watchdog timer reset";
			break;
		case 0x02:  
			mdata->cell_registered_p = 1;
			msg = "Registered with cellular network";
			net_mgmt_event_notify(NET_EVENT_IF_UP, 
					mdata->net_iface);
			break;
		case 0x03:
			mdata->cell_registered_p = 0;
			msg = "Unregistered with cellular network";
			net_mgmt_event_notify(NET_EVENT_IF_DOWN, 
					mdata->net_iface);
			break;
		case 0x0E:
			msg =  "Remote Manager connected";
			break;
		case 0x0F:
			msg =  "Remote Manager disconnected";
			break;
		case 0x32:
			mdata->ble_connect_p = 1;
			msg =  "BLE Connect";
			break;
		case 0x33:
			mdata->ble_connect_p = 0;
			msg =  "BLE Disconnect";
			break;
		case 0x34:
			msg =  "Bandmask configuration failed";
			break;
		case 0x35:
			msg =  "Cellular component update started";
			break;
		case 0x36:
			msg =  "Cellular component update failed";
			break;
		case 0x37: 
			msg = "Cellular component update completed";
			break;
		case 0x38: 
			msg = "XBee firmware update started";
			break;
		case 0x39:
			msg = "XBee firmware update failed";
			break;
		case 0x3A:
			msg =  "XBee firmware update applying";
			break;
		default:
			msg = "Unknown status";
			break;
	}

	mdata->ev_creg = frame_id;

	LOG_DBG("Modem status(0x%02X): %s", frame_id, msg);

	return 0;
}


/*
 * Modem Socket Command Handlers
 */
xb3_API_HANDLER(on_socket_create, 0xC0)
{
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	uint8_t status = data[1];
	int ret = 0;

	switch(status){
		case 0x22: //modem unregistered
			ret = ECONNREFUSED;	
			break;
		case 0x31: //internal error
			ret = ENETDOWN;
			break;
		case 0x32: //resource error
			ret = ENOSPC;
			break;
		case 0x7B: // invalid protocol
			ret = EPROTONOSUPPORT;
			break;
		case 0x7E: //modem update in progress
			ret = EBUSY;
			break;
		case 0x85: //unknown error
			ret = EIO;
			break;
		case 0x86: //invalid tls config
			ret = ENETDOWN;
			break;
		case 0x00:
			ret = 0;
			break;
		default:
			break;
	}

	if(!ret){
		struct modem_socket *sock = modem_socket_from_fd(
				&mdata->socket_cfg, FRAME_ID_TO_FD(frame_id));
		if(sock){
			sock->id = data[0];
			sock->data = mctx->driver_data;
			LOG_DBG("Socket (%d) successfully created with id %d",
					sock->sock_fd, sock->id);
		}
	}

	return ret;
}


xb3_API_HANDLER(on_socket_write, 0x89)
{
	int ret = 0;
	uint8_t status = data[0];
	ARG_UNUSED(mctx);

	switch(status){
		case 0x0: //success
			ret = 0;
			break;
		case 0x20://connection not found
			ret = ENOTCONN;
			break;
		case 0x21://tx to cell net failed
			ret = ENETUNREACH;
			break;
		case 0x22://not registered
			ret = ECONNREFUSED;
			break;
		case 0x2c://invalid frame
			ret = EINVAL;
			break;
		case 0x31://internal error
			ret = EIO;
			break;
		case 0x32://resource error
			ret = EIO;
			break;
		case 0x74://msg too long
			ret = EMSGSIZE;
			break;
		case 0x76://socket closed unexpectedly
			ret = ENOTCONN;
			break;
		case 0x78://invalid udp port
			ret = EINVAL;
			break;
		case 0x79://invalid tcp port
			ret = EINVAL;
			break;
		case 0x7a://invalid host addr
			ret = EINVAL;
			break;
		case 0x7b://invalid data mode
			ret = EINVAL;
			break;
		case 0x7c://invalid interface
			ret = EINVAL;
			break;
		default:
			break;
	}
	if(ret){
		LOG_DBG("Modem socket error on fd %d 0x%02X", 
				FRAME_ID_TO_FD(frame_id), status);
	}

	return ret;
}

static int close_sock_id(struct modem_context *mctx, uint8_t id);
static int offload_close(void *obj);

xb3_API_HANDLER(on_socket_connect, 0xC2)
{
	uint8_t id = data[0];
	uint8_t status = data[1];
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct modem_socket *sock = modem_socket_from_id(&mdata->socket_cfg, id);
	int ret = 0;

	switch(status){
		case 0x01: //invalid dest addr type
		case 0x02: //invalid addr/port
			ret = EADDRNOTAVAIL;	
			break;
		case 0x03: //connection in progress
		case 0x04: // already connected
			ret = EISCONN;
			break;
		case 0x05: //unknown error
			ret = EBUSY;
			break;
		case 0x20://invalid socket id
			ret = ENOTSOCK;
			break;
		case 0x00:
			sock->is_connected = 1;
			ret = 0;
			break;
		default:
			break;
	}

	return ret;
}

xb3_API_HANDLER(on_socket_close, 0xC3)
{
	uint8_t id = data[0];
	uint8_t status = data[1];
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct modem_socket *sock = modem_socket_from_id(&mdata->socket_cfg, id);
	if(!sock){
		return 0;
	}

	if(status != 0x20){
		modem_socket_put(&mdata->socket_cfg, sock->sock_fd);
		return 0;
	}else{
		return EBADF;
	}
}

xb3_API_HANDLER(on_socket_listen, 0xC6)
{
	uint8_t status = data[1];
	int ret = 0;
	ARG_UNUSED(mctx);

	switch(status){
		case 0x00: //Success
			ret = 0;
			break;
		case 0x01: //Invalid port
			ret = EINVAL;
			break;
		case 0x02://Error
			ret = EIO;
			break;
		case 0x03://Already bound or listening
			ret = EALREADY;
			break;
		case 0x20: //Invalid socket ID
			ret = EINVAL;
			break;
	}

	return ret;
}


static int copy_data_from_socket(struct modem_socket *sock,
		uint8_t *buf, uint16_t length)
{
	struct modem_data *mdata = (struct modem_data*)sock->data;
	struct socket_read_data *sock_data = &mdata->sock_data[sock->sock_fd];

	LOG_DBG("Reading %d bytes", length);
	int ret = ring_buf_get(&sock_data->rb, buf, length);
	
	if(ret > 0){
		sock_data->avail -= ret;
	}

	return ret;
}


static int copy_data_to_socket(struct modem_data *mdata, uint8_t id, 
		uint8_t *data, uint16_t length)
{
	int ret = 0;

	struct modem_socket *sock = modem_socket_from_id(&mdata->socket_cfg, id);
	if (!sock) {
		LOG_ERR("Socket not found! (%d)", id);
		ret = -EINVAL;
		goto exit;
	}

	LOG_DBG("Copying %d bytes", length);

	struct socket_read_data *sock_data = &mdata->sock_data[sock->sock_fd];


	int bytes_copied = 0;
	uint16_t bytes_remaining = length;
	uint8_t *p = data;
	do{
		ret = ring_buf_put(&sock_data->rb, p, bytes_remaining);
		if(ret < 0){
			LOG_ERR("Failed to write data to socket (%d)", ret);
			break;
		}
		p += ret;
		bytes_remaining -= ret;
		bytes_copied += ret;
		sock_data->avail += ret;
	}while(bytes_remaining > 0);

exit:
	return (ret >= 0) ? bytes_copied : ret;
}

//xbee3 will send this unsolicited whenever we receive 
//udp traffic on an open socket. This function simply records that information
//in a temporary buffer until someone calls recvfrom or a new packet arrives.
//It is unclear to me what to do in the cases where several packets arrive
//before the socket is read from... 
//TODO: figure that out
xb3_API_HANDLER(on_socket_recvfrom, 0xCE)
{
	int ret = 0;
	int payload_len = length;

	uint8_t id = data[0];
	data++;
	payload_len--;

	//addr/port come across in network order.
	uint32_t src_addr;
	uint16_t src_port;
	memcpy(&src_addr, data, sizeof(src_addr));
	data += sizeof(src_addr);
	payload_len-= sizeof(src_addr);

	memcpy(&src_port, data, sizeof(src_port));
	data += sizeof(src_port);
	payload_len -= sizeof(src_port);

	//one more byte for reserved status
	payload_len--;
	data++;

	//lookup socket
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct modem_socket *sock = modem_socket_from_id(&mdata->socket_cfg, id);
	if (!sock) {
		LOG_ERR("Socket not found! (%d)", id);
		goto exit;
	}


	struct socket_read_data *sock_data = &mdata->sock_data[sock->sock_fd];
	struct sockaddr_in *client_addr = net_sin(&sock_data->recv_addr);

	//leave bytes as they are. i guess its up to the user to flip em
	client_addr->sin_addr.s_addr = src_addr;
	client_addr->sin_port = src_port;

	LOG_DBG("Copying %d bytes", payload_len);

	ret = copy_data_to_socket(mdata, id, data, payload_len);
	if(ret < 0){
		ret = ENOMEM;
		goto exit;
	}else{
		ret = 0;
	}

	modem_socket_data_ready(&mdata->socket_cfg, sock);
exit:
	return ret;
}


//This frame is sent unsolicited when data arrives on a tcp socket
//same idea as on_socket_recvfrom except there's no addressing information
//to manage
xb3_API_HANDLER(on_socket_recv, 0xCD)
{
	uint8_t id = data[0];
	//uint8_t status = data[1];
	uint16_t payload_len =  length - 2;
	uint8_t *payload_ptr = &data[2];

	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct modem_socket *sock = modem_socket_from_id(
			&mdata->socket_cfg, id);

	int ret = copy_data_to_socket(mdata, id, payload_ptr, payload_len);
	if(ret < 0){
		ret = ENOMEM;
		goto exit;
	}
	modem_socket_data_ready(&mdata->socket_cfg, sock);
	ret = 0;

exit:
	return ret;
}


// This frame is sent whenever a socket we are listening on receives a new tcp
// connection.
xb3_API_HANDLER(on_socket_client, 0xCC)
{
	uint8_t id = frame_id;
	uint8_t client_id = data[0];


	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct modem_socket *sock = modem_socket_from_id(&mdata->socket_cfg, id);
	if(!sock){
		LOG_ERR("New Client: Bad socket id %d", id);
		return -1;
	}

	LOG_DBG("New client on socket %d id:%d client_id: %d", 
			sock->sock_fd, id, client_id);

	assert(sock->sock_fd < MDM_MAX_SOCKETS);

	struct socket_read_data *sock_data = &mdata->sock_data[sock->sock_fd];

	memcpy(net_sin(&sock_data->recv_addr)->sin_addr.s4_addr32, &data[1], 
			sizeof(uint32_t));
	memcpy(&net_sin(&sock_data->recv_addr)->sin_port, &data[5], 
			sizeof(uint16_t));

	net_sin(&sock_data->recv_addr)->sin_family = sock->family;

	sock_data->sock_id = client_id;

	k_sem_give(&sock_data->client_connection);

	return 0;
}

xb3_API_HANDLER(on_socket_status, 0xCF)
{
	int ret = 0;
	uint8_t id = frame_id;
	uint8_t status = data[0];

	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	struct modem_socket *sock = modem_socket_from_id(&mdata->socket_cfg, id);
	struct socket_read_data *sock_data = (struct socket_read_data*)sock->data;
	if(!sock){
		return 0;
	}

	switch(status){
		case 0x00: //successful connection
			sock->is_connected = 1;
			ret = 0;
			break;
		//every other case is fatal.
		case 0x01: //Failed DNS lookup
			ret = ENETUNREACH;
			break;
		case 0x02: //Connection refused
			ret = ECONNREFUSED;
			break;
		case 0x03: //Transport closed
			ret = ESHUTDOWN;
			break;
		case 0x04: //Timed out
			ret = ETIMEDOUT;
			break;
		case 0x05: //Internal error
			ret = EIO;
			break;
		case 0x06: //Host unreachable
			ret = EHOSTDOWN;
			break;
		case 0x07: //Connection lost
			ret = ECONNRESET;
			break;
		case 0x08: //Unknown error
			ret = EIO;
			break;
		case 0x09: //Unknown server
			ret = EHOSTDOWN;
			break;
		case 0x0A: //Resource error.
			//This error indicates a so called "socket leak" and
			//the modem must be reset.
			ret = ENETUNREACH;
			break;
		default:
			break;
	}

	//notify any waiting things about the error
	sock_data->avail = -ret;
	modem_socket_data_ready(&mdata->socket_cfg, sock);

	return ret;
}

xb3_API_HANDLER(on_at_response, 0x88)
{
	int ret = 0;
	struct modem_data *mdata = DATA_FROM_CTX(mctx);
	uint8_t status = data[2];
	char *value = &data[3];
	uint16_t val_len = length - 3;

	memset(mdata->at_buf, '\0', MDM_RECV_BUF_SIZE);
	//copy length to first two bytes
	memcpy(mdata->at_buf, &val_len, sizeof(val_len));
	memcpy(&mdata->at_buf[2], value, val_len);

	switch(status){
		case 0x00://ok
			ret = 0;
			break;
		case 0x01:
			ret = EIO;
			break;
		case 0x02:
		case 0x03:
		default:
			ret = EINVAL;
	}

	return ret;
}


static const struct xb3_api_handler *api_handlers[] = {
	&on_socket_recv,
	&on_socket_recvfrom,
	&on_socket_write,
	&on_socket_status,
	&on_socket_client,
	&on_socket_connect,
	&on_socket_listen,
	&on_socket_create,
	&on_at_response,
	&on_socket_close,
	&on_modem_status,
	NULL,
};

MODEM_CMD_DEFINE(xb3_api_handler)
{
	int pkt_len = 0;
	struct modem_data *mdata = &mdata_0;
	struct modem_context *mctx = &mdata->ctx;
	//copy the delim + length bytes
	size_t match_len = net_buf_linearize(
			data->match_buf,
			data->match_buf_len, 
			data->rx_buf, 
			0, 3);

	//wait for enough bytes to read length
	if(match_len != 3){
		return -EAGAIN;
	}


	pkt_len += match_len;

	uint16_t length = sys_be16_to_cpu(*((uint16_t*)&data->match_buf[1]));


	//attempt to linearize rest of packet
	match_len = net_buf_linearize(
			data->match_buf,
			data->match_buf_len,
			data->rx_buf,
			3, length + 1);

	//wait until entire packet is received
	if(match_len != length+1){
		return -EAGAIN;
	}


	pkt_len += match_len;

	uint8_t *frame = data->match_buf;
	uint8_t chksum = frame[length];
	uint8_t calc_chksum = calc_api_checksum(frame, length);

	//validate packet
	if(chksum != calc_chksum){
		LOG_DBG("frame chksum is not correct. Got %d expected %d",
				chksum, calc_chksum);
		//just bail out and signal the modem to throw out this packet
		goto bad_checksum;
	}

	//LOG_HEXDUMP_DBG(data->match_buf, MIN(10, match_len), "frame");

	k_sem_take(&data->sem_parse_lock, K_FOREVER);
	int err = EINVAL;
	for(int i = 0; i < ARRAY_SIZE(api_handlers); i++){
		const struct xb3_api_handler *h = api_handlers[i];
		if(!h){
			break;
		}
		if(h->frame_id == frame[0]){ 
			err = h->func(frame[1], &frame[2], length - 2, mctx);
			break;
		}
	}
	modem_cmd_handler_set_error(data, err);
	k_sem_give(&data->sem_parse_lock);

bad_checksum:
	k_sem_give(&mdata->sem_response);

	return pkt_len;
}

/*******************************************************************************
 * MODEM SOCKET FUNCTIONS
 ******************************************************************************/
static const struct socket_op_vtable offload_socket_fd_op_vtable;

static int offload_socket(struct modem_context *mctx, 
		int family, int type, int proto)
{
	int ret = 0;
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;

	ret = modem_socket_get(&mdata->socket_cfg, family, type, proto);
	if (ret < 0) {
		errno = -ret;
		LOG_ERR("could not get modem_socket");
		return -1;
	}

	uint8_t frame_id = FD_TO_FRAME_ID(ret);

	uint8_t xb3_sock_type;

	switch(type){
		case SOCK_STREAM:
			xb3_sock_type = 1;
			break;
		case SOCK_DGRAM:
			xb3_sock_type = 0;
			break;
		case SOCK_RAW:
			return -EINVAL;
			break;
		default:
			return -EINVAL;
	}

	struct net_pkt *buf = alloc_api_buf(mctx, 8);
	if(buf == NULL){
		LOG_ERR("failed to allocate net buf");
		errno = -ENOMEM;
		return -1;
	}

	net_pkt_write_u8(buf, 0x40);
	net_pkt_write_u8(buf, frame_id);
	net_pkt_write_u8(buf, xb3_sock_type);

	errno = send_api(mctx, buf, MDM_CMD_TIMEOUT);

	return ret;
}

static int close_sock_id(struct modem_context *mctx, uint8_t id){
	int ret = 0;
	struct net_pkt *buf = alloc_api_buf(mctx, 8);
	if(buf == NULL){
		errno = ENOMEM;
		return -1;
	}

	net_pkt_write_u8(buf, 0x43);
	net_pkt_write_u8(buf, 0x01);
	net_pkt_write_u8(buf, id);

	errno = send_api(mctx, buf, MDM_CMD_TIMEOUT);

	if(errno != 0){
		ret = -1;
	}
	
	return ret;
}

static int offload_close(void *obj)
{
	int ret = 0;
	struct modem_socket *sock = (struct modem_socket *)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;

	close_sock_id(&mdata->ctx, sock->id);

	modem_socket_put(&mdata->socket_cfg, sock->sock_fd);
	return ret;
}


static int offload_bind(void *obj, const struct sockaddr *addr,
			socklen_t addrlen)
{
	struct modem_socket *sock = (struct modem_socket *)obj;

	/* save bind address information */
	memcpy(&sock->src, addr, sizeof(*addr));

	struct modem_data *mdata = (struct modem_data*)sock->data;

	struct net_pkt *buf = alloc_api_buf(&mdata->ctx, 16);
	if(buf == NULL){
		errno = ENOMEM;
		return -1;
	}

	net_pkt_write_u8(buf, 0x46);
	net_pkt_write_u8(buf, FD_TO_FRAME_ID(sock->sock_fd));
	net_pkt_write_u8(buf, sock->id);
	net_pkt_write(buf, &net_sin(&sock->src)->sin_port, 2);

	errno = send_api(&mdata->ctx, buf, MDM_CMD_TIMEOUT);

	return errno ? -1 : 0;
}

static int offload_listen(void *obj, int backlog)
{
	struct modem_socket *sock = (struct modem_socket *)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;
	struct socket_read_data *sock_data = &mdata->sock_data[sock->sock_fd]; 
	int ret = k_sem_init(&sock_data->client_connection, 0, backlog);

	if(ret){
		LOG_ERR("Failed to listen on socket %d (%d)", sock->id, ret);
		return -1;
	}
	
	return 0;
}


static int offload_accept(void *obj, struct sockaddr *addr, socklen_t *addrlen)
{
	struct modem_socket *sock = (struct modem_socket *)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;
	struct socket_read_data *sock_data = &mdata->sock_data[sock->sock_fd];
	int ret = 0;
	int id = -1;


	//block here until a new client comes in
	k_sem_take(&sock_data->client_connection, K_FOREVER);

	memcpy(addr, &sock_data->recv_addr, sizeof(struct sockaddr));

	*addrlen = sizeof(sock_data->recv_addr);

	id = sock_data->sock_id;

	//create a socket object that the listener can use to do things
	ret = modem_socket_get(&mdata->socket_cfg, 
			sock->family, 
			sock->type, 
			sock->ip_proto);

	struct modem_socket *client = modem_socket_from_fd(
			&mdata->socket_cfg, ret);

	//copy parent data over
	memcpy(&client->src, &sock->src, sizeof(struct sockaddr));
	memcpy(&client->dst, addr, sizeof(struct sockaddr));

	client->id = id;
	client->is_connected = 1;
	client->data = mdata;

	return ret;
}


static int offload_connect(void *obj, const struct sockaddr *addr,
			   socklen_t addrlen)
{
	int ret = 0;
	struct modem_socket *sock = (struct modem_socket *)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;

	if(sock->id < mdata->socket_cfg.base_socket_num - 1){
		LOG_ERR("Invalid socket_id(%d) from fd:%d", 
				sock->id, sock->sock_fd);
		errno = -EINVAL;
		return -1;
	}

	//this modem only does ipv4
	if(addr->sa_family == AF_INET){
		memcpy(&sock->dst, addr, sizeof(*addr));
	}else{
		errno = -EAFNOSUPPORT;
		return -1;
	}

	//skip connect for udp
	if(sock->ip_proto == IPPROTO_UDP){
		errno = 0;
		return 0;
	}

	struct net_pkt *buf = alloc_api_buf(&mdata->ctx, 16);
	if(buf == NULL){
		errno = ENOMEM;
		return -1;;
	}

	net_pkt_write_u8(buf, 0x42);
	net_pkt_write_u8(buf, 0x01);
	net_pkt_write_u8(buf, sock->id);
	net_pkt_write(buf, &net_sin(addr)->sin_port, 2);

	//dest type
	//0 = binary ipv4 in network byte order
	//1 = string of dotted ip address or domain name
	net_pkt_write_u8(buf, 0);
	net_pkt_write(buf, &net_sin(addr)->sin_addr.s_addr, 4);

	errno = send_api(&mdata->ctx, buf, MDM_CMD_TIMEOUT);

	if(errno != 0){
		LOG_ERR("socket connect failed ret %d", errno);
		ret = -1;
	}else{
		sock->is_connected = 1;
		ret = 0;
	}

	return ret;
}

//the modem will send data we receive unsolicited. therefore, we need only
//check to see if data is in the socket
static ssize_t offload_recvfrom(void *obj, void *buf, size_t len,
				int flags, struct sockaddr *from,
				socklen_t *fromlen)
{
	struct modem_socket *sock = (struct modem_socket *)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;
	struct socket_read_data *sock_data = 
		&mdata->sock_data[sock->sock_fd];
	
	int next_packet_size;

	if(!buf || len == 0){
		errno = EINVAL;
		return -1;
	}

	next_packet_size = sock_data->avail;

	//if we have no data, either wait for some or return
	if(!next_packet_size){
		if(flags & ZSOCK_MSG_DONTWAIT){
			errno = EAGAIN;
			return -1;
		}

		if(!sock->is_connected && sock->ip_proto != IPPROTO_UDP){
			LOG_DBG("sock not connected.. returning");
			errno = ENOTCONN;
			return -1;
		}

		modem_socket_wait_data(&mdata->socket_cfg, sock);
		next_packet_size = sock_data->avail;
	}

	if(next_packet_size < 0){
		errno = next_packet_size;
		return -1;
	}

	size_t copy_size = MIN(len, next_packet_size);

	int ret = copy_data_from_socket(sock, buf, copy_size);
	if(ret < 0){
		LOG_ERR("failed to copy data from socket buffer");
		errno = ENOMEM;
		goto exit;
	}
	
	errno = 0;

	if(from && fromlen){
		*fromlen = sizeof(sock->dst);
		memcpy(from, &sock_data->recv_addr, *fromlen);
	}

	LOG_DBG("read %d/%d bytes requested", copy_size, len);

	//pull data from socket if peek flag isn't set
	if((flags & ZSOCK_MSG_PEEK)){
		LOG_DBG("Just peeking. IF YOU SEE THIS YOU HAVE A BUG TO FIX");
	}

exit:


	return errno ? -1 : copy_size;
}

static ssize_t offload_write(void *obj, const void *buffer, size_t count)
{
	struct modem_socket *sock = (struct modem_socket *)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;

	if (!buffer || count == 0) {
		errno = EINVAL;
		return -1;
	}

	if (!sock->is_connected && sock->ip_proto != IPPROTO_UDP) {
		errno = ENOTCONN;
		return -1;
	}

	struct net_pkt *buf = alloc_api_buf(&mdata->ctx, count + 5);
	if(!buf){
		errno = ENOMEM;
		return -1;
	}

	count = MIN(MDM_MAX_DATA_LENGTH, count);

	net_pkt_write_u8(buf, 0x44);
	net_pkt_write_u8(buf, 0x01);
	net_pkt_write_u8(buf, sock->id);
	net_pkt_write_u8(buf, 0x00);

	net_pkt_write(buf, buffer, count);

	errno = send_api(&mdata->ctx, buf, K_FOREVER);

	return errno ? -1 : count;
}

static ssize_t offload_sendto(void *obj, const void *buf, size_t len,
			      int flags, const struct sockaddr *to,
			      socklen_t tolen)
{
	struct modem_socket *sock = (struct modem_socket *)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;


	if (!buf || len == 0) {
		errno = EINVAL;
		return -1;
	}

	if (!sock->is_connected && sock->ip_proto != IPPROTO_UDP) {
		LOG_DBG("Sendto called on non udp socket");
		errno = ENOTCONN;
		return -1;
	}

	if (!to ) {
		if(sock->ip_proto == IPPROTO_UDP){
			to = &sock->dst;
		}else{
			return offload_write(obj, buf, len);
		}
	}


	len = MIN(MDM_MAX_DATA_LENGTH, len);

	struct net_pkt *netbuf = alloc_api_buf(&mdata->ctx, len + 10);
	if(!netbuf){
		errno = ENOMEM;
		return -1;
	}

	net_pkt_write_u8(netbuf, 0x45);
	net_pkt_write_u8(netbuf, 0x01);
	net_pkt_write_u8(netbuf, sock->id);

	net_pkt_write(netbuf, &net_sin(to)->sin_addr.s_addr, 4);
	net_pkt_write(netbuf, &net_sin(to)->sin_port, 2);

	net_pkt_write_u8(netbuf, 0x00);
	net_pkt_write(netbuf, buf, len);

	errno = send_api(&mdata->ctx, netbuf, MDM_CMD_TIMEOUT);

	return errno ? -1 : len;
}

static int offload_poll(struct zsock_pollfd *fds, int nfds, int msecs)
{
	int i;
	void *obj = NULL;

	/* Only accept modem sockets. */
	for (i = 0; i < nfds; i++) {
		if (fds[i].fd < 0) {
			continue;
		}

		/* If vtable matches, then it's modem socket. */
		obj = z_get_fd_obj(fds[i].fd,
				   (const struct fd_op_vtable *)
						&offload_socket_fd_op_vtable,
				   EINVAL);
		if (obj == NULL) {
			return -1;
		}
	}

	struct modem_socket *sock = (struct modem_socket*)obj;
	struct modem_data *mdata = (struct modem_data*)sock->data;

	assert(mdata != NULL);

	return modem_socket_poll(&mdata->socket_cfg, fds, nfds, msecs);
}

static int offload_ioctl(void *obj, unsigned int request, va_list args)
{
	switch (request) {
	case ZFD_IOCTL_POLL_PREPARE:
		return -EXDEV;

	case ZFD_IOCTL_POLL_UPDATE:
		return -EOPNOTSUPP;

	case ZFD_IOCTL_POLL_OFFLOAD: {
		struct zsock_pollfd *fds;
		int nfds;
		int timeout;

		fds = va_arg(args, struct zsock_pollfd *);
		nfds = va_arg(args, int);
		timeout = va_arg(args, int);

		return offload_poll(fds, nfds, timeout);
	}

	default:
		errno = EINVAL;
		return -1;
	}
}

static ssize_t offload_read(void *obj, void *buffer, size_t count)
{
	ssize_t ret = offload_recvfrom(obj, buffer, count, 0, NULL, NULL);
	return ret;
}


static ssize_t offload_sendmsg(void *obj, const struct msghdr *msg, int flags)
{
	ssize_t sent = 0;
	int rc;

	LOG_DBG("msg_iovlen:%d flags:%d", msg->msg_iovlen, flags);

	for (int i = 0; i < msg->msg_iovlen; i++) {

		const char *buf = msg->msg_iov[i].iov_base;
		size_t len = msg->msg_iov[i].iov_len;

		while (len > 0) {
			rc = offload_sendto(obj, buf, len, flags,
							msg->msg_name,
							msg->msg_namelen);
			if (rc < 0) {
				if (rc == -EAGAIN) {
					k_sleep(MDM_SENDMSG_SLEEP);
				} else {
					sent = rc;
					break;
				}
			} else {
				sent += rc;
				buf += rc;
				len -= rc;
			}
		}
	}

	return (ssize_t)sent;
}

static const struct socket_op_vtable offload_socket_fd_op_vtable = {
	.fd_vtable = {
		.read = offload_read,
		.write = offload_write,
		.close = offload_close,
		.ioctl = offload_ioctl,
	},
	.bind = offload_bind,
	.connect = offload_connect,
	.sendto = offload_sendto,
	.recvfrom = offload_recvfrom,
	.listen = offload_listen,
	.accept =  offload_accept,
	.sendmsg = offload_sendmsg,
	.getsockopt = NULL,
	.setsockopt = NULL,
};

static bool offload_is_supported(int family, int type, int proto)
{
	/* TODO offloading always enabled for now. */
	return true;
}


static int net_offload_dummy_get(sa_family_t family,
				 enum net_sock_type type,
				 enum net_ip_protocol ip_proto,
				 struct net_context **context)
{

	LOG_ERR("CONFIG_NET_SOCKETS_OFFLOAD must be enabled for this driver");

	return -ENOTSUP;
}

/* placeholders, until Zepyr IP stack updated to handle a NULL net_offload */
static struct net_offload modem_net_offload = {
	.get = net_offload_dummy_get,
};

/*******************************************************************************
 * MODEM INFO FUNCTIONS
 ******************************************************************************/
#define query_phone_num(context)	\
	send_api_at(context, "PH", DATA_FROM_CTX(context)->mdm_ph_num, MDM_PH_LENGTH)
#define query_imei(context) 	 	\
	send_api_at(context, "IM", DATA_FROM_CTX(context)->mdm_imei, MDM_IMEI_LENGTH)
#define query_iccid(context) 		\
	send_api_at(context, "II", DATA_FROM_CTX(context)->mdm_iccid, MDM_ICCID_LENGTH)
#define query_imsi(context) 		\
	send_api_at(context, "S#", DATA_FROM_CTX(context)->mdm_imsi, MDM_IMSI_LENGTH)


static int query_ip(struct modem_context *mctx)
{
	struct in_addr addr;

	int ret = send_api_at(mctx, "MY", addr.s4_addr, 4);
	if(ret){
		return ret;
	}

	return 0;
}


/*******************************************************************************
 * HOUSE KEEPING
 ******************************************************************************/
/* RX thread */
static void modem_rx(void *p1, void *p2, void *p3)
{
	struct modem_context *mctx = (struct modem_context*)p1;
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;
	ARG_UNUSED(p2);
	ARG_UNUSED(p3);

	while (true) {
		/* wait for incoming data */
		k_sem_take(&mdata->iface_data.rx_sem, K_FOREVER);

		mctx->cmd_handler.process(&mctx->cmd_handler, &mctx->iface);

		/* give up time if we have a solid stream of data */
		k_yield();
	}
}

static void modem_rssi_query(struct modem_context *mctx)
{
	struct modem_data *data = DATA_FROM_CTX(mctx);

	//can't query anything until we're connected
	if(data->ev_creg != 0x02){
		return;
	}

	if(data->mdm_imei[0] == '\0'){
		query_imei(mctx);
		query_iccid(mctx);
		query_imsi(mctx);
		query_phone_num(mctx);
	}
	int ret;
	char resp[2];
	char param = 0;

	/* query modem RSSI */
	ret = send_api_at_param(mctx, "DB", &param, 1, resp, 2);

	if (ret < 0) {
		mctx->data_rssi = -1000;
	}else{
		int8_t rssi = (int8_t)resp[0];
		mctx->data_rssi = -rssi;
	}
}



static int pin_init(struct modem_context *mctx)
{
	LOG_INF("Initializing pins");

	struct modem_pin *pin = &mctx->pins[MDM_RESET];
	LOG_DBG("MDM_RESET Pin: %s:%d", pin->dev_name, pin->pin);
	if(pin->gpio_port_dev){
		LOG_DBG("MDM Reset -> ASSERTED");
		int ret = modem_pin_write(mctx, MDM_RESET, 1);
		if(ret){
			LOG_ERR("Failed to set RESET %s:%d (%d)", 
					pin->dev_name, pin->pin, ret);	
			return -1;
		}

		k_sleep(K_SECONDS(1));

		LOG_DBG("MDM Reset -> NOT ASSERTED");
		ret = modem_pin_write(mctx, MDM_RESET, 0);
		if(ret){
			LOG_ERR("Failed to set RESET %s:%d (%d)", 
					pin->dev_name, pin->pin, ret);	
			return -1;
		}
	}

	pin = &mctx->pins[MDM_DTR];
	if(pin->gpio_port_dev){
		LOG_DBG("MDM DTR -> NOT ASSERTED");
		int ret = modem_pin_write(mctx, MDM_DTR, 0);
		if(ret){
			LOG_ERR("Failed to set DTR (%s:%d) (%d)", 
					pin->dev_name, pin->pin, ret);	
			return -1;
		}
	}


	LOG_INF("... Done!");
	return 0;
}


static int enter_cmd_mode(struct modem_context *mctx, uint32_t guard_time)
{
	int ret;
	struct modem_data *mdata = (struct modem_data*)mctx->driver_data;

	k_sem_take(&mdata->cmd_handler_data.sem_tx_lock, K_FOREVER);
	
	LOG_DBG("Entering cmd mode...");
	/* modem requires some amount of silence 
	 * before and after +++ to be happy.
	 * default is 1 sec */
	k_sleep(K_SECONDS(guard_time));

	ret = mctx->iface.write(&mctx->iface, "+++", 3);

	k_sem_reset(&mdata->sem_response);
	ret = k_sem_take(&mdata->sem_response, K_SECONDS(guard_time * 5));

	if(ret){
		LOG_ERR("FAILED enter cmd mode (%d)", ret);
	}

	k_sem_give(&mdata->cmd_handler_data.sem_tx_lock);

	return ret;
}


static int find_baud(struct modem_context *mctx)
{
	int ret;
	int i;
	for(i = 0; i < ARRAY_SIZE(xb3_baud_rates); i++){
		LOG_DBG("Trying baud rate: %d", xb3_baud_rates[i]);

		ret = modem_iface_uart_set_baud(&mctx->iface, 
				xb3_baud_rates[i]);	
		FAIL_IF_ERR(ret, -1, "Failed to set baud rate %d", ret);

		ret = enter_cmd_mode(mctx, 1);
		if(!ret){
			break;
		}
	}

	return (i == ARRAY_SIZE(xb3_baud_rates)) ? -1 : 0;
}


static int modem_reset(struct modem_context *mctx)
{
	int ret = 0;
	int retry_count = 0;
	struct modem_data *mdata = DATA_FROM_CTX(mctx);

	static struct setup_cmd setup_cmds[] = {
		//enable api mode
		SETUP_CMD_NOHANDLE("ATAP1"), 
		//exit cmd mode
		SETUP_CMD_NOHANDLE("ATCN"), 
	};

restart:
	ret = pin_init(mctx);
	GOTO_IF_ERR(ret, error, "Pin init failed %d", ret);


	LOG_INF("Waiting for modem to respond...");

	/* Give the modem a while to start responding to AT cmd.
	 * If we've already tried, assume the baud rate is incorrect. */
	ret = (retry_count == 0) ? 
		enter_cmd_mode(mctx, 1) :
		find_baud(mctx);

	GOTO_IF_ERR((ret), error, "Failed to enter cmd mode %d", ret);

	LOG_INF("OK!");
	retry_count = 0;

	ret = modem_cmd_handler_setup_cmds(&mctx->iface, &mctx->cmd_handler,
					   setup_cmds, ARRAY_SIZE(setup_cmds),
					   &mdata->sem_response,
					   MDM_REGISTRATION_TIMEOUT);

	GOTO_IF_ERR((ret < 0), error, "Failed to send setup at commands");

	//close all sockets that may or may not already be open on a modem.
	//This can happen if the board is reset but the modem remains powered.
	//this probably doesn't hurt anything.
	for(int i = 0; i < MDM_MAX_SOCKETS; i++){
		close_sock_id(mctx, i);
	}

	LOG_INF("Modem initialized successfully");
	return 0;

error:
	if(retry_count++ < MDM_NETWORK_RETRY_COUNT){
		LOG_ERR("Restarting (%d/%d)", 
				retry_count, MDM_NETWORK_RETRY_COUNT);
		goto restart;
	}

	LOG_ERR("Modem failed to initialize.");
	return ret;
}


#define HASH_MULTIPLIER		37
static uint32_t hash32(char *str, int len)
{
	uint32_t h = 0;
	int i;

	for (i = 0; i < len; ++i) {
		h = (h * HASH_MULTIPLIER) + str[i];
	}

	return h;
}

static inline uint8_t *modem_get_mac(const struct device *dev)
{
	struct modem_data *mdata = (struct modem_data*)dev->data;
	uint32_t hash_value;

	mdata->mac_addr[0] = 0x00;
	mdata->mac_addr[1] = 0x10;

	/* use IMEI for mac_addr */
	hash_value = hash32(mdata->mdm_imei, strlen(mdata->mdm_imei));

	UNALIGNED_PUT(hash_value, (uint32_t *)(mdata->mac_addr + 2));

	return mdata->mac_addr;
}

static void modem_net_iface_init(struct net_if *iface)
{
	const struct device *dev = net_if_get_device(iface);
	struct modem_data *data = dev->data;

	/* Direct socket offload used instead of net offload: */
	iface->if_dev->offload = &modem_net_offload;
	net_if_set_link_addr(iface, modem_get_mac(dev),
			     sizeof(data->mac_addr),
			     NET_LINK_ETHERNET);
	data->net_iface = iface;
	return;
}

static struct net_if_api api_funcs = {
	.init = modem_net_iface_init,
};

static struct modem_cmd response_cmds[] = {
	MODEM_CMD("OK", on_cmd_ok, 0U, ""),
	MODEM_CMD("ERROR", on_cmd_error, 0U, ""),
	MODEM_CMD_DIRECT("~", xb3_api_handler),
};

static struct modem_cmd unsol_cmds[] = {
	MODEM_CMD_DIRECT("~", xb3_api_handler),
};

static int modem_init(const struct device *dev)
{
	int ret = 0;

	struct modem_data *mdata = (struct modem_data*)dev->data;
	struct modem_config *mcfg = (struct modem_config*)dev->config;
	struct modem_context *mctx = &mdata->ctx;

	LOG_INF("Initializing modem");

	k_sem_init(&mdata->sem_response, 0, 1);

	/* socket config */
	ret = modem_socket_init(&mdata->socket_cfg,
				&offload_socket_fd_op_vtable);
	GOTO_IF_ERR((ret < 0), error,
			"Failed to initialize modem socket offload (%d)", ret);

	//initialize ring buffers for each socket
	for(int i = 0; i < MDM_MAX_SOCKETS; i++){
		struct socket_read_data *s = &mdata->sock_data[i];
		ring_buf_init(&s->rb, sizeof(s->buf), s->buf);
	}


	/* cmd handler */
	ret = modem_cmd_handler_init(
			&mctx->cmd_handler, &mdata->cmd_handler_data);
	GOTO_IF_ERR((ret < 0), error, 
			"Failed to register cmd handler (%d)", ret);

	/* modem interface */
	ret = modem_iface_uart_init(&mctx->iface, 
			&mdata->iface_data, mcfg->bus_name);
	GOTO_IF_ERR((ret < 0), error, "Failed to init bus %s", mcfg->bus_name);

	/* modem data storage */
	snprintk(mdata->mdm_man, sizeof(mdata->mdm_man), 
			"DIGI");
	snprintk(mdata->mdm_model, sizeof(mdata->mdm_model), 
			"xbee 3");
	snprintk(mdata->mdm_rev, sizeof(mdata->mdm_rev), 
			"LTE-M/NB-IoT Global");

	ret = modem_context_register(mctx);
	GOTO_IF_ERR((ret < 0), error, 
			"Error registering modem context: %d", ret);

	/* start RX thread */
	k_tid_t tid = k_thread_create(&mdata->thread_data, mdata->rx_stack,
			CONFIG_MODEM_XB3_RX_STACK_SIZE,
			(k_thread_entry_t) modem_rx,
			mctx, NULL, NULL, 
			K_PRIO_COOP(7), 0, K_NO_WAIT);


	char thread_name[16];
	snprintf(thread_name, sizeof(thread_name), "%s_rx", dev->name);
	k_thread_name_set(tid, thread_name);

	ret = modem_reset(mctx);

	if(ret){
		LOG_ERR("Error resetting modem");
	}

error:
	return ret;
}

#define XB3_NONE(inst) 

#define XB3_DTR_PIN(inst)  							\
 	.pins[MDM_DTR] = {  							\
		.dev_name = DT_INST_GPIO_LABEL(inst, dtr_gpios),  	 	\
		.pin = DT_INST_GPIO_PIN(inst, dtr_gpios),			\
		.init_flags = DT_INST_GPIO_FLAGS(inst, dtr_gpios)| GPIO_OUTPUT	\
	},
#define XB3_RESET_PIN(inst)  							\
 	.pins[MDM_RESET] = {  							\
		.dev_name = DT_INST_GPIO_LABEL(inst, reset_gpios),  		\
		.pin = DT_INST_GPIO_PIN(inst, reset_gpios),			\
		.init_flags = DT_INST_GPIO_FLAGS(inst, reset_gpios)|GPIO_OUTPUT	\
	},


#define XB3_POPULATE_DTR(inst) 							\
	COND_CODE_1(DT_INST_NODE_HAS_PROP(inst, dtr_gpios),			\
			(XB3_DTR_PIN(inst)), 					\
			(XB3_NONE(inst)))

#define XB3_POPULATE_RESET(inst) 						\
	COND_CODE_1(DT_INST_NODE_HAS_PROP(inst, reset_gpios), 			\
			(XB3_RESET_PIN(inst)), 					\
			(XB3_NONE(inst)))


#define XB3_SIM_NUMBERS(inst) 							\
	.data_imsi = mdata_##inst.mdm_imsi, 					\
	.data_iccid = mdata_##inst.mdm_iccid, 					


#define XB3_POP_SIM_NUMERS(inst) 						\
	IF_ENABLED(CONFIG_MODEM_SIM_NUMBERS, (XB3_SIM_NUMBERS(inst))) 					

#define XB3_INST_BUS_PROP(inst, prop) 						\
	DT_PROP(DT_BUS(DT_DRV_INST(inst)), prop)


#define XB3_QUERY_WORK(inst) 							\
/* specialize query work to modem context */ 					\
static void modem_rssi_query_work_##inst(struct k_work *work) 			\
{ 										\
 	struct modem_context *ctx = &mdata_##inst.ctx;				\
	struct modem_data *data = DATA_FROM_CTX(ctx); 				\
	modem_rssi_query(ctx); 							\
 										\
	/* re-start RSSI query work */ 						\
	k_delayed_work_submit(&data->rssi_query_work,  				\
			K_SECONDS(CONFIG_MODEM_XB3_RSSI_PERIOD));		\
} 										\
static int modem_rssi_query_start_##inst(const struct device *dev) 		\
{ 										\
	struct modem_data *data = &mdata_##inst; 				\
	k_delayed_work_init(&data->rssi_query_work,  				\
			(k_work_handler_t)modem_rssi_query_work_##inst);	\
	modem_rssi_query_work_##inst(&data->rssi_query_work); 			\
	return 0; 								\
} 										\
SYS_INIT(modem_rssi_query_start_##inst, POST_KERNEL, 0); 			


#define XB3_INSTANTIATE(inst) 							\
NET_BUF_POOL_DEFINE(mdm_pool_##inst,MDM_RECV_MAX_BUF,MDM_RECV_BUF_SIZE,0,NULL);	\
										\
static struct modem_data mdata_##inst = { 					\
	.iface_data = { 							\
		.hw_flow_control = XB3_INST_BUS_PROP(inst, hw_flow_control),	\
		.rx_rb_buf = mdata_##inst.mdm_iface_rb,				\
		.rx_rb_buf_len = sizeof(mdata_##inst.mdm_iface_rb),		\
	}, 									\
	.cmd_handler_data = { 							\
		.cmds[CMD_RESP] = response_cmds, 				\
		.cmds_len[CMD_RESP] = ARRAY_SIZE(response_cmds), 		\
		.cmds[CMD_UNSOL] = unsol_cmds, 					\
		.cmds_len[CMD_UNSOL] = ARRAY_SIZE(response_cmds), 		\
		.match_buf = mdata_##inst.mdm_match_buf, 			\
		.match_buf_len = sizeof(mdata_##inst.mdm_match_buf),		\
		.eol = "\r", 							\
		.eol_len = 1, 							\
		.buf_pool = &mdm_pool_##inst, 					\
		.alloc_timeout = K_NO_WAIT, 					\
	}, 									\
	.socket_cfg = { 							\
		.sockets = mdata_##inst.sockets,				\
		.sockets_len = ARRAY_SIZE(mdata_##inst.sockets),		\
		.base_socket_num = MDM_BASE_SOCKET_NUM, 			\
	}, 									\
	.ctx = {			 					\
		.driver_data = &mdata_##inst,	 				\
		.pins = mdata_##inst.pins,					\
		.pins_len = ARRAY_SIZE(mdata_##inst.pins), 			\
		.data_manufacturer = mdata_##inst.mdm_man,			\
		.data_model = mdata_##inst.mdm_model,				\
		.data_revision = mdata_##inst.mdm_rev,				\
		.data_imei = mdata_##inst.mdm_imei,				\
		XB3_POP_SIM_NUMERS(inst) 					\
	},								 	\
	XB3_POPULATE_RESET(inst) 						\
	XB3_POPULATE_DTR(inst) 							\
}; 										\
 										\
static struct modem_config mcfg_##inst = { 					\
	.bus_name = DT_INST_BUS_LABEL(inst), 					\
	.baud = XB3_INST_BUS_PROP(inst, current_speed), 			\
}; 				 						\
										\
 	 	 	 	 						\
NET_DEVICE_DT_INST_OFFLOAD_DEFINE(inst, modem_init, device_pm_control_nop, 	\
		&mdata_##inst, &mcfg_##inst, 					\
		CONFIG_MODEM_XB3_INIT_PRIORITY, &api_funcs, 			\
		MDM_MAX_DATA_LENGTH);						\
 	 	 	 	 						\
/*specialize offload_socket to modem context */ 				\
static int offload_socket_##inst(int family, int type, int proto) 		\
{ 	 	 	 	 						\
	return offload_socket(&mdata_##inst.ctx, family, type, proto);		\
} 	 	 	 	 						\
 	 	 	 	 						\
NET_SOCKET_REGISTER(xbee3_lte_##inst, AF_INET, offload_is_supported, 		\
		    offload_socket_##inst); 					\

DT_INST_FOREACH_STATUS_OKAY(XB3_INSTANTIATE)
